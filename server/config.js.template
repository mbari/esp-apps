// Export the configuration
module.exports = {

    // These are configurations that are specific to the server.js application
    server: {
        // This is the base directory where the data for the portal is stored. It is a path
        // located on the server where the server.js is being run. It is used in the AppServer.js
        // script to serve as static web resources. It is also used by the DataAccess.js
        // script to locate the images when creating HTTP accessible links to them and
        // also so it knows where on the local machine to write the CSV versions of the
        // ancillary data files
        dataDir: './data',
        // This is the location (relative to the host running the server.js script)
        // where log files for the various components will be written.
        logDir: './logs',
        // This is the base URL where the API will be served.  You might have to change
        // this if you are using a proxy pass to expose this to the outside world.
        apiBaseUrl: 'http://localhost:8081',
        // This used by the AppServer.js script to tell the Express server what port
        // to listen to
        port: 8081,
        // This is the username that will be used to define who the Slack messages are
        // coming from
        slackUsername: '',
        // This is the URL where the Slack messages will be pushed to.
        slackWebHookURL: '',
        // Properties used by DataAccess.js
        dataAccessOptions: {
            // This is the host where the CouchDB instance is running. This will likely
            // change depending on how you have deployed the various services. It needs
            // to be a hostname that can be resolved by the server.js process
            couchHost: 'localhost',
            // The port that CouchDB is listening to
            couchPort: 5984,
            // Whether or not the connection to CouchDB is over SSL
            couchSSL: false,
            // The username that will be used by DataAccess to connect to the CouchDB server
            couchUsername: '',
            // The password that will be used by DataAccess to connect to the CouchDB server
            couchPassword: '',
            // This is the name of the CouchDB that will be used
            couchDatabase: 'esp',
            // This is the connection protocol to connect to the PostgreSQL DB (probably should
            // not be changed)
            pgProtocol: 'postgres',
            // This is the host where the PostgreSQL instance is running. This will likely
            // change depending on how you have deployed the various services. It needs
            // to be a hostname that can be resolved by the server.js process
            pgHost: 'localhost',
            // This is the port the PostgreSQL server is listening on
            pgPort: 5432,
            // This is the username DataAccess will use to connect to the PostgreSQL
            pgUsername: '',
            // This is the password DataAccess will use to connect to the PostgreSQL
            pgPassword: '',
            // This is the name of the database that will be used by DataAccess
            pgDatabase: 'esp_ancillary',
            // This is the number of data points to put into a batch load when DataAccess
            // recieves a request to post some ancillary data points.
            numAncillaryPointsToBatch: 1000
        },
        // These are just the levels of logging that can be configured for the various components
        // Valid options are: off, fatal, error, warn, info, debug, trace, all
        loggingLevels: {
            server: 'info',
            dataAccess: 'info',
            appServer: 'info'
        }
    },

    // These are configurations that are specific to the crawler.js application and relative to
    // where that application is running
    crawler: {
        // This is used by the crawler (DeploymentFileSync) and is a path located on the
        // host that is running the crawler and is used to know where to write the files
        // it is pulling down from the remote FTP server
        dataDir: './data',
        // This is the location (relative to the host running the server.js script)
        // where log files for the various components will be written.
        logDir: './logs',
        // This is the base of the URL where the crawler will query for open deployments. It
        // needs to be relative to where the crawler.js is running and reachable by that process
        apiBaseUrl: 'http://localhost:8081',
        // This is an interval (in milliseconds) that the crawler will wait between each run
        // of syncing FTP files
        ftpSyncIntervalMillis: 60000,
        // These are just the levels of logging that can be configured for the various components
        // Valid options are: off, fatal, error, warn, info, debug, trace, all
        loggingLevels: {
            crawler: 'info',
            deploymentFileSync: 'info'
        }
    },

    // These are configuration that are specific to the parseDeployments.js application and relative
    // to where that application is running
    parseDeployments: {
        // This is used by the parser so that it knows where to look for files during the parsing
        dataDir: './data',
        // This is the location (relative to the host running the server.js script)
        // where log files for the various components will be written.
        logDir: './logs',
        // This is the base of the URL where the parser will query for open deployments. It
        // needs to be relative to where the parseDeployments.js is running and reachable by that process
        apiBaseUrl: 'http://localhost:8081',
        // This is a look up table that is used by the OutParser to associate more details about
        // ancillary data that is parsed out of the log files.  It uses the keys of the source
        // of the data (Can, CTD, etc) and the unit found attached to the data value to look up
        // long variable names and full units.
        ancillaryDataLookup: {
            CTD: {
                'C': {
                    varName: 'Temp',
                    varLongName: 'Temperature',
                    units: 'Degrees C'
                },
                'm': {
                    varName: 'Depth',
                    varLongName: 'Depth',
                    units: 'meters'
                },
                'psu': {
                    varName: 'Sal',
                    varLongName: 'Salinity',
                    units: 'psu'
                },
                'mg/m^3': {
                    varName: 'Chl',
                    varLongName: 'Chlorophyll',
                    units: 'mg/m^3'
                },
                '%': {
                    varName: 'Light Tx',
                    varLongName: 'Light Transmission',
                    units: '%'
                },
                'ml/L': {
                    varName: 'Diss O2',
                    varLongName: 'Computed Dissolved Oxygen',
                    units: 'ml/L'
                },
                'decibars': {
                    varName: 'Press',
                    varLongName: 'Pressure',
                    units: 'decibars'
                },
                'S/m': {
                    varName: 'Cond',
                    varLongName: 'Conductivity',
                    units: 'S/m'
                }
            },
            Can: {
                'C': {
                    varName: 'Temp',
                    varLongName: 'Temperature',
                    units: 'Degrees C'
                },
                '% Wet!': {
                    varName: '% Wet!',
                    varLongName: 'Percent Wet',
                    units: '%'
                },
                '% humidity': {
                    varName: '% Humidity',
                    varLongName: 'Percent Humidity',
                    units: '%'
                },
                'psia': {
                    varName: 'Press',
                    varLongName: 'Pressure',
                    units: 'psia'
                },
                'V': {
                    varName: 'Volt',
                    varLongName: 'Battery Voltage',
                    units: 'V'
                },
                'A': {
                    varName: 'Inst Curr',
                    varLongName: 'Instantaneous Current',
                    units: 'A'
                },
                'A avg': {
                    varName: 'Avg Curr',
                    varLongName: 'Average Current',
                    units: 'A'
                },
                'W': {
                    varName: 'Power',
                    varLongName: 'Power',
                    units: 'W'
                },
                '% flow': {
                    varName: '% Flow',
                    varLongName: 'Percent Flow',
                    units: '%'
                },
                'L/min': {
                    varName: 'Flow',
                    varLongName: 'Flow rate',
                    units: 'L/min'
                }
            },
            ISUS: {
                'uM/L no^3': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM/L no^3'
                },
                'uM/L no3': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM/L no3'
                },
                'uM no^3': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM no^3'
                },
                'uM no3': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM no3'
                },
                'uM/L hs': {
                    varName: 'Nitrate 2',
                    varLongName: 'Nitrate 2',
                    units: 'uM/L hs'
                },
                'uM hs': {
                    varName: 'Nitrate 2',
                    varLongName: 'Nitrate 2',
                    units: 'uM hs'
                },
                'psu': {
                    varName: 'PSU',
                    varLongName: 'PSU',
                    units: 'psu'
                }
            },
            SatlanticISUS: {
                'uM/L': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM/L'
                },
                'psu': {
                    varName: 'PSU',
                    varLongName: 'PSU',
                    units: 'psu'
                }
            },
            SUNA: {
                'uM': {
                    varName: 'Nitrate',
                    varLongName: 'Nitrate',
                    units: 'uM'
                },
                'psu': {
                    varName: 'Salinity',
                    varLongName: 'Salinity',
                    units: ''
                }
            }
        },
        // These are just the levels of logging that can be configured for the various components
        // Valid options are: off, fatal, error, warn, info, debug, trace, all
        loggingLevels: {
            parseDeployments: 'info',
            outParser: 'info',
            deploymentUtils: 'info'
        }
    }
};
